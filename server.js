const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const ethers = require('ethers');
var EthWallet = ethers.Wallet;
const Cryptr = require('cryptr');
const utils = ethers.utils;
const Web3 = require('web3');
const Tx = require('ethereumjs-tx');

app.use(express.static('public'));
const urlencodedParser = bodyParser.urlencoded({ extended: false });

/*****get ethereum wallet address******/
app.get('/createEthAddress', urlencodedParser, function(req, res) {
  var data;
  
  //generate ether wallet address and privateKey
  //var addr = new EthWallet('0x7ba5ba9bfbdc2795b3c038c3fe78390580bf8eb3a2413bcd44e42191eaf6e4f6');
  //res.status(200).send(addr);
  //return;
  
  var ethwallet = EthWallet.createRandom();
  var Addr = ethwallet.address;
  var alength = Addr.length;
  var cryptr = new Cryptr(Addr.slice(0,2)+Addr.slice((alength/2),((alength/2)+2))+Addr.slice((alength-2),alength));
  var priv_key = cryptr.encrypt(ethwallet.privateKey);
  
  var web3 = new Web3(new Web3.providers.HttpProvider("http://ftechiz.com:8585/"));
  data = web3.eth.accounts.create();
  res.status(200).send(data);
  return;
  
  data = {
	  'privateKey': priv_key,
	  'address': Addr
  }
  res.status(200).send(data);
});

/*****ethereum wallet transaction******/
app.post('/ethTransactoin', urlencodedParser, function(req, res) {
  var data;
  var priv_key = req.body.priv_key;
  var fromAddr = req.body.fromAddr;
  var toAddr = req.body.toAddr;
  var amount = req.body.amount;
  var Network = req.body.Network;
  
  //decrypt private key
  var alength = fromAddr.length;
  var cryptr = new Cryptr(fromAddr.slice(0,2)+fromAddr.slice((alength/2),((alength/2)+2))+fromAddr.slice((alength-2),alength));
  var priv_key = cryptr.decrypt(priv_key);
  var wallet = new EthWallet(priv_key);
  
  var providers = ethers.providers;
  
  
  //set the network type
    var networkProv;
    if(Network == 'mainnet'){
	   //main network
	   networkProv = providers.networks.mainnet;
	} else {
	   //ropsten test network
	   networkProv = providers.networks.ropsten
	   //networkProv = "http://192.168.1.106:30303/";
	}
	
    //ropsten test network
    //var ropstenNetwork = providers.networks.ropsten;

	// Connect to Etherscan
	var etherscanProvider = new providers.EtherscanProvider(networkProv);
	wallet.provider = etherscanProvider;

	var transaction = {
		gasLimit: 21000,
		gasPrice: 20000000000,
		to: toAddr,
		data: "0x",
		value: ethers.utils.parseEther(amount)
	};

	// Send the transaction
	var sendTransactionPromise = wallet.sendTransaction(transaction);

	sendTransactionPromise.then(function(transactionHash){
	  console.log(transactionHash);
	  data = {
		  'status': '1',
		  'msg': 'Ether successfully transferred',
		  'txhash': transactionHash.hash,
	  }
	  res.status(200).send(data);
	});
});

/*****ethereum token transaction******/
app.post('/ethTokenTransfer', urlencodedParser, function(req, res) {
  var priv_key = req.body.priv_key;
  var fromAddr = req.body.fromAddr;
  var toAddr = req.body.toAddr;
  var tokens = req.body.tokens;
  var contractAddress = req.body.contractAddress;
  var Network = req.body.Network;
  
  //decrypt private key
    var alength = fromAddr.length;
    var cryptr = new Cryptr(fromAddr.slice(0,2)+fromAddr.slice((alength/2),((alength/2)+2))+fromAddr.slice((alength-2),alength));
    priv_key = cryptr.decrypt(priv_key);
    priv_key = priv_key.slice(2,priv_key.length);
    
	//set the network type
	var networkProv;
    if(Network == 'mainnet'){
	   //main network
	   networkProv = "https://mainnet.infura.io";
	} else {
	   //ropsten test network
	   //networkProv = "https://ropsten.infura.io";
	   networkProv = "http://192.168.1.106:30303";
	}
	
	//add web3 providers
	var web3 = new Web3(new Web3.providers.HttpProvider(networkProv));
	
	//add abi of token contract from etherscan
	//var abi = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"totalSupply","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balances","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"_totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}];
	
	var count = web3.eth.getTransactionCount(fromAddr);
	
	//get token abi
	var abi = require('human-standard-token-abi');
	var abiArray = abi;
	
	//var contractAddress = "0xd7C9dDeADAB94474899c0AD87dBC4118576D505A";
	
	//define contract
	var contract = web3.eth.contract(abiArray).at(contractAddress);
    var decimal = contract.decimals();
	
	//set tokens for transaction 
	tokens = tokens * Math.pow(10, decimal);
	
	var data = contract.transfer.getData(toAddr, tokens);
	var gasPrice = 20000000000;
	var gasLimit = 90000;
    
	//set transaction variable
	var rawTransaction = {
	  "from": fromAddr,
	  "nonce": web3.toHex(count),
	  "gasPrice": web3.toHex(gasPrice),
	  "gasLimit": web3.toHex(gasLimit),
	  "to": contractAddress,
	  "value": 0,
	  "data": data,
	  //"chainId": 0x03
	};
	
	//get private key
	var privKey = new Buffer(priv_key, 'hex');
	var tx = new Tx(rawTransaction);
	
	//sign transaction
	tx.sign(privKey);
	var serializedTx = tx.serialize();
	
	var jsondata;
	//send transaction	
	web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err, hash){
	  if(!err){
		  jsondata = { 
			  'status': '1',
			  'msg': 'Token successfully transferred',
			  'txhash': hash
		  }
		  res.status(200).send(jsondata);
	  } else {
		 jsondata = { 
			  'status': '0',
			  'msg': 'error sending ether, Please try again'
		  }
		 res.status(200).send(jsondata);
	  }
	});
	
});

/*****get ethereum balance******/
app.post('/getEthBalance', urlencodedParser, function(req, res) {
	var data;
	
	//get address from request
	var addr = req.body.address;
	var Network = req.body.Network;
	
	//var providers = ethers.providers;
 
  /**************Get the ethereum Balance**************/

	//set the network type
    /*var networkProv;
    if(Network == 'mainnet'){
	   //main network
	   networkProv = providers.networks.mainnet;
	} else {
	   //ropsten test network
	   networkProv = providers.networks.ropsten
	}*/
	
	//var web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io"));
	var web3 = new Web3(new Web3.providers.HttpProvider("http://ftechiz.com:8585/"));
	var balance = web3.eth.getBalance(addr);
	res.status(200).send(balance);
	return;
	//ropsten test network
	//var ropstenNetwork = providers.networks.ropsten;
	
	//main network
	//var mainNetwork = providers.networks.mainnet;

	// Connect to Etherscan
	var etherscanProvider = providers.getDefaultProvider(networkProv);
    
	//run get ethereum balance function
	var balancePromise = etherscanProvider.getBalance(addr);
	balancePromise.then(function(balance){
	  var etherString = ethers.utils.formatEther(balance);
	    //add value to data to send response
		data = {
			'status': '1',
			'etherBalance': etherString
		}
		
		//send response
		res.status(200).send(data);
	}); 
  
});

/*****get ethereum balance******/
app.post('/getTokenBalance', urlencodedParser, function(req, res) {
	var data;
	
	//get address from request
	var addr = req.body.address;
	var contractAddress = req.body.contractAddress;
	
/**************Get the ethereum Token Balance**************/ 
    //set the network type
	var networkProv;
    if(Network == 'mainnet'){
	   //main network
	   networkProv = "https://mainnet.infura.io";
	} else {
	   //ropsten test network
	   networkProv = "https://ropsten.infura.io";
	}
	//add web3 providers
	//var web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io"));
	
	//mainet provider
	var web3 = new Web3(new Web3.providers.HttpProvider(networkProv));

	//get abi of token
	var abi = require('human-standard-token-abi');
	var abiArray = abi;
	
	//GET test token contract address
	//var contractAddress = "0x63091244180Ae240C87D1F528f5F269134cB07B3";
	
	//obo token contract address
    //var contractAddress = "0x36D2dc450CA70E492f7013870Fa13575d4cae582";
	
	//define contract
	var tokenContract = web3.eth.contract(abiArray).at(contractAddress);
	var decimal = tokenContract.decimals();
	var tokenBalance = tokenContract.balanceOf(addr);
		tokenBalance = tokenBalance / Math.pow(10, decimal);
	var tokenName = tokenContract.name();
	var tokenSymbol = tokenContract.symbol();
	
	var tokenDetail = {
			  'tokenBalance': tokenBalance,
			  'tokenName': tokenName,
			  'tokenSymbol': tokenSymbol
		 }
   res.status(200).send(tokenDetail);
  
});

//set server port
var server = app.listen(7272, function() {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port);
})